$(function() {
	var formOcr = $('#form-ocr');
	var formSearch = $('#form-search');

	formOcr.on('submit', function (event) {
		event.preventDefault();
		var url_value = $('input[name="url"]').val();
		$.ajax({
			url: 'http://localhost:5000/v1/ocr',
			type: 'POST',
			contenType: 'application/json',
			data: formOcr.serialize()
		})
		.done(function(data) {
			console.log(data);
			$('#output-section').css('display', 'block');
			$('#output').text(data.output)
		})
		.fail(function(error) {
			console.log(error);
		})
		.always(function() {
			console.log("complete");
		});
	});

	formSearch.on('submit', function (event) {
		event.preventDefault();
		var word = $('input[name="input-search"]').val();
		$.ajax({
			url: 'http://localhost:5000/v1/ocr?word='+word,
			type: 'GET',
			contenType: 'application/json',
		})
		.done(function(data) {
			console.log(data);
			$('#images-section').css('display', 'block');
			$.each(data.images, function(index, value) {
				$('#images').append('<li class="col-md-4"><img class="img-responsive" src="'+value+'"></li>');
			});
			$('#images li').css('list-style-type', 'none');
		})
		.fail(function(error) {
			console.log(error);
		})
		.always(function() {
			console.log("complete");
		});
	});

});